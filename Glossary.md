[Wiki Home Page](./Home.md)

# Glossary of Terms and Concepts

*Please add to this page! Use headings to group definitions into categories
and use lists for organizing terms and definitions under such headings.
For example,*

## Induction and Inference Rules

- **Axiom**

    an assertion held to be true without precondition (or hypothesis)

- **Inference Rule**

    a rule used in to derive or prove the truth of some assertion (called the "conclusion") based on the assumption of truth of one or more other assertions (called "hypotheses")


## Formal Grammars

- **Context-Free Grammar**

    a formal grammar in which each production for a non-terminal is valid in any context where the non-terminal appears.

- **Formal Grammar**

    a set of substitution rules (called *productions*) that describe how any valid "sentence" of a natural/programming language can be constructed from its "alphabet"

- **Non-Terminal Symbol**

    a placeholder symbol that does not directly represent any particular concrete symbol, and which must be substituted in a syntactic derivation; contrast with *Terminal Symbol*

- **Production**

    a rule that specifies a valid substitution of one sequence of symbols in place of another

- **Terminal symbols**

    concrete characters that appear directly in the external representation of a language

## Formal Semantics

- **Axiomatic Semantics**

    an approach to specifying the behavior of a programming language that relies on provable logic statements about expressions/statements in a program

- **Denotational Semantics**

    an approach to specifying the behavior of a programming language in which expressions/statements are represented by mathematical functions/objects

- **Operational Semantics**

    an approach to specifying the behavior of a programming language that focuses on the *computational sequences* required to determine the "value" of a program (or specific expressions/statements of a program)

- **Value, Denoted**

    any value that may be bound to a variable in the environment; the set of denoted values is often, but not always, the same as the set of expressed values

- **Value, Expressed**

    any value that may be the result of evaluating an expression written in the source language; the set of all expressed values is usually described as the union of the sets of values for each data type available in the source language

## Interpreters

- __Host (or Defining) Language__

    the language that is used to implement a source language; for example, we could write a C++ program that interprets Python code, in which case C++ is the host/defining language

- __Source (or Defined) Language__

    a language that we use to write programs, which must be compiled or interpreted in order to execute those programs; in this course, the LET-language and others from the text can be the source language
