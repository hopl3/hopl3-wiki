# Environment in Java

Below you can find some example implementations of the environment abstraction in more familiar language like Java.

You may want to look at [Expressed Values in Java](ExpValJava) for more context.

## Data Structure Representation

Java already has support for data structures with named fields via Classes, so we do not need the kind of kludgy representation that the authors' present in Scheme.

```
#!java
/**
 * One possible object-oriented approach to the environment abstraction.
 */
public class Environment {

    /**
     * Empty environment doesn't contain any variable bindings, so we'll use the null reference.
     */
    public static final Environment EMPTY = null;

    private String savedVar;
    private ExpVal savedVal;
    private Environment savedEnv;

    /**
     * Using a constructor as our "extend-env" method.
     * Extending creates an environment that adds a variable-value binding on top of an existing environment.
     * Note that we're not changing the existing environment - it is 'inside' of the extended environment.
     * @param  x  a variable name
     * @param  v  a value
     * @param  f  an environment
     */
    public Environment(String var, ExpVal val, Environment env) {
        this.savedVar = var;
        this.savedVal = val;
        this.savedEnv = env;
    }

    /**
     * Applying an environment means looking-up the value bound to a variable name.
     * Here, our environments are objects, so we just use object field lookup.
     * @note We could make this an instance method instead, but then trying to apply an empty environment
     *       would yield a NullPointerException instead of the more specific exception we use here.
     * @param  f  an environment
     * @param  x  a variable name
     */
    public static ExpVal apply(Environment f, String x) throws Exception {
        if (f == null)
            throw new Exception("Error: No binding found for variable '" + x + "'");
        if (f.savedVar == x)
            return f.savedVal;
        else
            return apply(f.savedEnv, x);
    }

}
```

Since the interface is identical to that above, the exact same client-code works:

```
#!java
Environment env1 = Environment.EMPTY;
Environment env2 = new Environment("x", new NumVal(10), env1);
Environment env3 = new Environment("y", new NumVal(5), env2);
// need try-catch, or the current context must throw
try {
	ExpVal val = Environment.apply(env2, "x"); // val will be NumVal(10)
	Environment.apply(env3, "z");
} catch (Exception e) {
	// ...
}
```

