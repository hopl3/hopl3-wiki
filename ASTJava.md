# Abstract Syntax Trees in Java

Below you can find some example implementations of AST nodes in more familiar language like Java.

You may want to look at [Expressed Values in Java](ExpValJava) and [Environments in Java](EnvironJava) for more context.

## The Representation

In the object-oriented style, we might come up with something like this to represent our abstract syntax:

```
#!java
/**
 * One possible object-oriented approach to abstract syntax nodes.
 * An empty interface is used only so that we have a common base type for all expressions.
 */
public interface Expression {}

/**
 * Represents a number literal expression.
 * @note We can use public final members to simulate the immutable values of Haskell.
 */
public class ConstExp implements Expression {
    public ConstExp(int num) {
        this.num = num;
    }
    public final int num;
}

/**
 * Represents a variable reference expression.
 * @note We can use public final members to simulate the immutable values of Haskell.
 */
public class VarExp implements Expression {
    public VarExp(String var) {
        this.var = var;
    }
    public final String var;
}

/**
 * Represents a difference (i.e., subtraction) expression.
 * @note We can use public final members to simulate the immutable values of Haskell.
 */
public class DiffExp implements Expression {
    public DiffExp(Expression exp1, Expression exp2) {
        this.rand1 = exp1;
        this.rand2 = exp2;
    }
    public final Expression rand1;
    public final Expression rand2;
}

/**
 * Represents an expression that compares a number against zero.
 * @note We can use public final members to simulate the immutable values of Haskell.
 */
public class IsZeroExp implements Expression {
    public IsZeroExp(Expression exp1) {
        this.rand = exp1;
    }
    public final Expression rand;
}

/**
 * Represents an if-then-else conditional expression.
 * @note We can use public final members to simulate the immutable values of Haskell.
 */
public class IfExp implements Expression {
    public IfExp(Expression exp1, Expression exp2, Expression exp3) {
        this.test   = exp1;
        this.conseq = exp2;
        this.altern = exp2;
    }
    public final Expression test;
    public final Expression conseq;
    public final Expression altern;
}

/**
 * Represents an expression for implementing local variable declarations.
 * @note We can use public final members to simulate the immutable values of Haskell.
 */
public class LetExp implements Expression {
    public LetExp(String var, Expression exp1, Expression exp2) {
        this.var  = var;
        this.rhs  = exp1;
        this.body = exp2;
    }
    public final String     var;
    public final Expression rhs;
    public final Expression body;
}

```

Although we would typically have a parser generate our AST from a source program, we can manually build an AST for testing purposes if we wish:

```
#!java
    Expression prog = new LetExp(
        "x",
        new ConstExp(5),
        new LetExp(
            "y",
            new DiffExp(
                new ConstExp(10),
                new VarExp("x")
            ),
            new IsZeroExp(
                new DiffExp(
                    new VarExp("y"),
                    new ConstExp(1)
                )
            )
        )
    );

    if (prog instanceof LetExp) { // does this particular AST node represent a let-expression?
        System.out.println(((LetExp)prog).var); // just a little test
    }

```
