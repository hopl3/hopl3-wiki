# Happy Grammars

## Basic Syntax

Happy understands a syntax for context-free grammars that is similar to BNF. Consider the following example of a simple grammar in the form that Happy understands.

    #!happy
    Exp : Number            { ConstExp $1 }
        | "-" "(" Exp "," Exp ")"  { DiffExp $3 $5 }

Here we have a non-terminal _Exp_ that has two productions. All the way to the right on each line is the _abstract syntax expansion_ - this defines how the Haskell value that will be used to represent each production (or, each "kind of Exp" if you want to think of it like that).

In the expansion, we can refer to symbol from the right-hand side of a production by using a positional parameter. For example, in the expansions above, the positional parameters are `$1`, `$3`, and `$5` - indicating the first, third, and fifth symbols, respectively.

In this example, the names `ConstExp` and `DiffExp` are data constructors for some AST node data type that we must have defined previously. It is possible to use build-in Haskell operations and constructors also, as shown in the next section.

## Lists of Non-terminals

_Happy_ does not have any notation equivalent to the Kleene star or plus. Instead, we can define productions using the same approach outlined in our textbook on page 7 in the _ListOfInt_ example.

Here is what the _Happy_ grammar for such a non-terminal would look like:

    #!happy
    ListOfInt : "(" ")"                    { [] }
              | "(" Int "." ListOfInt ")"  { $2 : $4 }

Note that we can use nearly any Haskell expression for our abstract syntax constructions. Here, when encountering an empty _ListOfInt_, we just return Haskell's empty list. Next, if we have a non-empty _ListOfInt_, we can use the Haskell cons operator to build a Haskell list from the second (`Int`) and fourth (`ListOfInt`) symbols in the production.