# Problem Solving Tips

## Debugging

1. _Avoid confusing __not the only error__ with __not an error__._

    Example: Your code fails to run. Someone points out an error, and you fix
    it. You code still fails to run. This does not mean that the thing you just
    fixed was not an error... it just means that is was not the only error. You
    are now one fewer error away from code that runs. Don't give up.

2. _..._

## Structural Decomposition

_..._