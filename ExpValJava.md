# Expressed Values in Java

Below you can find an example implementation of the expressed value abstraction in more familiar language like Java.

## Object-Oriented Representation

```
#!java
/**
 * Base class of all expressed values.
 */
public interface ExpVal {
    default public int     toNum () throws Exception { throw new Exception("ExpVal: Not a NumVal!"); }
    default public boolean toBool() throws Exception { throw new Exception("ExpVal: Not a BoolVal!"); }
}

/**
 * Specialization of expressed value that represents a number.
 */
public class NumVal implements ExpVal {
    public  NumVal(int n) { this.num = n; }
    @Override
    public  int toNum() { return this.num; }
    private int num;
}

/**
 * Specialization of expressed value that represents a boolean.
 */
public class BoolVal implements ExpVal {
    public  BoolVal(boolean z) { this.bool = z; }
    @Override
    public  boolean toBool() { return this.bool; }
    private boolean bool;
}
```

And now some usage examples...

```
#!java
ExpVal val1 = new NumVal(5);
ExpVal val2 = new BoolVal(true);
// need try-catch or the current context must throw
try {
	int num1 = val1.toNum();
	boolean bool2 = val2.toBool();
} catch (Exception e) {
	// ...
}
```

