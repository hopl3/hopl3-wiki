[Wiki Home Page](./Home.md)

Understanding Haskell by Example
================================

This script contains myriad examples of functions that demonstrate important
details of Haskell syntax as well principles of the functional programming
paradigm. I hope you find it helpful.

Since this page is part of our course Wiki repository, please feel free to add
new Sections with your own examples. I have stuck to Markdown syntax for all
prose, so please use this convention yourself if you add content to this page.

This page is provided in two forms: Markdown and Literate Haskell. If you
choose to contribute to it, please be sure to add your changes to both
versions. Click here for the [Markdown version](HaskellBasics) that you can read
easily in your Web browser.

---

First Functions
---------------

Much the way some programming languages treat everything as an object, in
Haskell we should consider everything to be a function.

For example

> lukesFather :: String
> lukesFather = "Darth Vader"

is best thought of as a zero-argument function that always returns the string
"Darth Vader". Sometimes we get lazy and call it a variable and think of it
sort of like simple variables in other languages that we have experience with.
But in reality, this is just a zero-argument "constant function". The type
declaration is optional since Haskell can easily deduce that `lukesFather`
returns a String, but it is  good practice to always declare our types.

What about functions that take an argument? A simple example of such a
function is

> f :: Int -> Int
> f x = x^2 + 2*x + 1

Haskell includes a variety of recognizable operators in its Prelude module (a
default module that is loaded automatically). Note that the type declaration
is longer and includes an arrow `->`. The leftmost type name always indicates
the argument (i.e., input) type. We can think of the rightmost type name as
the final result type, though this is actually a little white lie, as we will
discover later. So the type `Int -> Int` actually describes a function that
takes in an integer and returns an integer.

To apply such a function, we must provide a specific argument value. We need
not enclose the argument in parentheses as in mathematical notation (though we
could if we so desired).

> y = f 5  -- y will be 5^2+2*5+1 = 36, same as f (x)

How about a somewhat more interesting function:

> power :: Int -> Int -> Int
> power x n = if n == 0 then 1 else x * (power x (n-1))

This is a big step above the first two functions. The `power` function
requires two inputs, the base and the exponent. We can read its type
`Int -> Int -> Int` as saying just this: that the first argument is an Int,
the second argument is an Int also, and the final result is yet another Int.

Yet, there is another interesting characteristic of `power`, namely that it is
defined recursively. The *base case* occurs when (n == 0), and the *general
case* reduces inductively toward that.

It is possible to write the function about in several different but equivalent
forms. Here we show each of these, noting that the `case-of` syntax is the most
primitive form; all others may be considered to be *syntactic sugar*. We omit
the type declarations in the interest of brevity.

> -- version using pattern-matching with the case-of syntax
> power_caseof x n = case n of 0 -> 1
>                              _ -> x * (power x (n-1))
>
> -- version using pattern-matching in the function declaration
> power_patterns x 0 = 1
> power_patterns x n = x * (power x (n-1))
>
> -- version using boolean guarded expressions (i.e., guards)
> power_guarded x n | n == 0    = 1
>                   | otherwise = x * (power x (n-1))

Note that when a value is not referenced, it can be ommitted and replaced by a
placeholder, `_`.

---

Second Element of a List
------------------------

Next let's consider the problem of finding the second element of a list.

Thinking through the problem, not every list will have a second element. We
can identify a few distinct cases:

- The input list is empty, no second element.
- The input list has just one element (i.e., it is a *singleton* list). Again,
    no second element.
- The input list contans at least two elements... there *is* a second element.

What do we do for the two cases in which no second element exists? We can simply fail by throwing an error using the `error` function.

> secondElem :: [a] -> a
> secondElem xs =
>     if null xs then error "Empty list has no second element!"
>     else if null (tail xs) then error "Singleton has no second element!"
>          else head (tail xs)

Using the alternate forms of `case-of`, patterns in the function declaration,
or guards, we might have written `secondElem` in anhy of the following styles:

> -- version using pattern-matching with the case-of syntax
> -- in this case-of form we are providing names for all parameters
> secondElem' xs = case xs of []  -> error "Empty list has no second element!"
>                             [x] -> error "Singleton has no second element!"
>                             (x2:x1:xs) -> head (tail xs)
>
> -- version using pattern-matching in the function declaration
> -- note that here we demonstrate omitting names for unused parameters
> secondElem'' []  = error "Empty list has no second element!"
> secondElem'' [_] = error "Singleton has no second element!"
> secondElem'' (_:x2:_) = x2
>
> -- version using boolean guarded expressions (i.e., guards)
> -- note that guarded expressions must be boolean-valued
> secondElem''' xs | null xs        = error "Empty list has no second element!"
>                  | null (tail xs) = error "Singleton has no second element!"
>                  | otherwise      = head (tail xs)

Note that here we have omitted unused parameter names and instead replaced them
with underscore `_` as a placeholder.

---

Euclid's Algorithm
------------------

Another example of simple recursion is Euclid's algorithm for computing the
greatest common divisor (GCD) of two integers.

> euclid :: Int -> Int -> Int
> euclid a b = if b == 0 then a else euclid b (a `mod` b)

Note that the `mod` function in Haskell compute the remainder, much like the
`%` operator in many other programming languages. In this code, by surrounding
`mod` in *backquotes* we can use it as an *infix* function (like symbolic
operatorss). We could have written the modulus operation in the usual prefix
style as `mod a b`, without the backquotes.

Now let's consider alternate constructions once again. In the first version we
show the prefix form of the `mod` function.

> -- version using pattern-matching with the case-of syntax
> euclid' a b = case b of 0 -> a
>                         _ -> euclid b (mod a b)
>
> -- version using pattern-matching in the function declaration
> euclid'' a 0 = a
> euclid'' a b = euclid b (a `mod` b)
>
> -- version using boolean guarded expressions (i.e., guards)
> euclid''' a b | b == 0    = a
>               | otherwise = euclid b (a `mod` b)

---

Recursion and Lists
-------------------

Lists are one of the most familiar and commonly used types in Haskell. It is
a good idea to practice writing recursive functions involving lists.

Our first example here will be to comute the length of a list. Of course,
Haskell Prelude already includes a `length` function to do just this.
Nevertheless, we feel it is a goo example to work through.

What will be the type of this function? Well, it takes in a list (of any type
of thing) and returns an integer (the count of how many elements in the list).

> list_length :: [a] -> Int
> list_length xs = if null xs then 0
>                  else 1 + (list_length (tail xs))

---

Avoding Too Many Parentheses
----------------------------

In some expressions, parentheses are required to override the normal function
or operator precedence. Occassionally, too many parentheses can become an
obstacle to readability.

A useful trick in Haskell is to employ the `$` operator in order to avoid
dealing with parentheses. The `$` operator is, in a nutshell, a function of
lowest precedence that simply passes its right-hand operand as an argument to
its left-hand operand. This operator can be defined as:

    infixr 0 $
    ($) :: (a -> b) -> a -> b
    f $ x = f x

The first line if this code tells Haskell that this operator _(a)_ is an infix
function, _(b)_ is right-associative, and _(c)_ has zero/lowest precedence.

Let's see how we can use this function in re-write of `secondElem`, which we
will call `secondElement`.

> secondElement :: [a] -> a
> secondElement xs =
>     if null xs then error "Empty list has no second element!"
>     else if null $ tail xs then error "Singleton has no second element!"
>          else head $ tail xs

---
