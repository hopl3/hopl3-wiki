# An End-to-End Example

Suppose that we wish to add a new expression to the _LET_ language that looks something like this:

```
twice(5)
```

which evaluates to a NumVal that represents 10.

## Test Cases

A good place to start is to write some useful test programs that use the new expression: at least one simple case, and another test that uses the new expression in the context of some existing larger expression. We write these in the __Test/LET.hs__ file, following the example of other existing test cases:

```
#!haskell
    NumTest "simple-twice" "twice(5)" 10,
    NumTest "twice-in-let" "let y = twice(x) in -(y,1)" 19,
    NumTest "twice-in-if"  "if zero?(x) then -(x,1) else twice(x)" 20
```

## Syntax

### Lexical Specification

To describe the syntax, we first we need to add "twice" as a new lexical token in the language. So we add the following lines to LexSpec.x.

```
#!haskell
    "twice"    { \s -> TTwice }
```

```
#!haskell
    | TToken
```

### Grammatical Specification

Next, we must add the appropriate production to our grammar. In the abstract syntax, we will identify our production as a "twice-expression". There is just one sub-expression non-terminal on the right-hand side (which will yield the value to be doubled).

We add the following new kind of node to the __AST.hs__ file:

```
#!haskell
    | TwiceExp Exp
```

and then we add the grammatical production to the __Grammar.y__ file as:

```
#!haskell
    | "twice" "(" Exp ")"  { TwiceExp $3 }
```

## Semantics

Once the syntactic rules are complete, we can turn our attention to the semantic rules.

We should first write out an inference rule in the host-agnostic style; we can do this inside a comment in the source file initially, but I recommend removing that comment before you commit your changes se we don't clutter up the code with too many large comments.

```
#!haskell
    {-
            <exp1,p> => ⌈n⌉
        ------------------------
        <twice(exp1),p> => ⌈2×n⌉
    -}
```

Once we have the formal rule specified, we can begin converting it into a host-language-specific style that includes the AST node names and ExpVal types:

```
#!haskell
    {-
               (NumVal n) = value_of exp1 p
        -------------------------------------------
        value_of (TwiceExp exp1) p = NumVal (2 * n)
    -}
```

A final implementation involves moving the hypotheses down underneath the conclusion using a Haskell _where_-clause, like so:


```
#!haskell
        value_of (TwiceExp exp1) p = NumVal (2 * n)
            where (NumVal n) = value_of exp1 p
```

That is the whole solution, since the `twice` operation does not require changes to any of the supporting structures (like ExpVal or Environment).