[Wiki Home Page](./Home.md)

# Defining Language Syntax with Alex + Happy

**Important Note:** *The HOPL3 codebase includes a shell script `build.sh`
that automates the compile/build procedure, include the steps for scanner
and parser generation described below.*

## Lexical Specification

For each model language included in this codebase, we define the lexical
specification in the **LexSpec.x** file. To modify the lexical specification
(e.g., add/remove/change rules for new lexical tokens) for one of the
included model languages, find and edit the appropriate **LexSpec.x** file.

For example, suppose we wish to change the *PROC* language so that functions
are defined using keyword **func** instead of the original **proc** keyword.
Edit the *PROC/LexSpec.x* file to change the following rule

```
#!alex
    "proc"                { \s -> TProc }
```

into this

```
#!alex
    "func"                { \s -> TProc }
```

(We could also rename the token from **TProc** to **TFunc**, but that is
unnecessary.)

## Generating a Lexical Analyzer

We use *Alex* to generate the lexical analyzer for a language. For example,
to re-generate the lexical analyzer for the *LET* language, run the following
command from within the top-level directory (*not* from inside the *LET*
directory):

```
#!bash
$ alex LET/LexSpec.x -o LET/Scanner.hs
```

## Grammatical Specification

For each model language included in this codebase, we define the grammar in
the **Grammar.y** file. To modify the grammar (i.e., add/remove/change the
production for an expression or statement) for one of the included model
languages, find and edit the corresponding **Grammar.y** file.

For example, suppose that we wish to change the *PROC* language so that
function definitions are written as `proc x -> -(x,10)`, instead of as
`proc (x) -(x,10)`. In addition to adding the right-arrow token `->` to
our lexical specification, we must also edit the *PROC/Grammar.y* file to
change the rule for *ProcExp*.

```
#!happy
    | proc '(' var ')' Exp     { ProcExp $3 $5 }
```

into this

```
#!happy
    | proc var '->' Exp        { ProcExp $2 $4 }
```

(We could also rename the token from **TProc** to **TFunc**, but that is
unnecessary.)

## Generating a Parser

We use *Happy* to generate the parser for a language. For example, to build
or re-build the parser for the *LET* language, run the following command
from within the top-level directory (*not* from inside the *LET*
directory itself):

```
#!bash
$ happy LET/Grammar.y -o LET/Parser.hs
```

