# Environment in JavaScript

Below you can find some example implementations of the environment abstraction in more familiar language like JavaScript.

You may want to look at [Expressed Values in JavaScript](ExpValJS) for more context.

## Procedural/Functional Representation

JavaScript functions are first-class, much like Scheme's procedures, so it is possible to implement a functional representation of environments.

```
#!javascript
/**
 * Empty environment doesn't contain any variable bindings, so always generate an error
 * @param  x  a variable name
 */
function emptyEnv(x) {
    throw "Error: No binding found for variable '" + x + "'";
}

/**
 * Applying an environment means looking-up the value bound to a variable name.
 * Here, our environments are just functions, we call the function.
 * @param  f  an environment
 * @param  x  a variable name
 */
function applyEnv(f, x) {
    return f(x);
}

/**
 * Extending creates an environment that adds a variable-value binding on top of an existing environment.
 * Note that we're not changing the existing environment - it is 'inside' of the extended environment.
 * @param  x  a variable name
 * @param  v  a value
 * @param  f  an environment
 */
function extendEnv(x, v, f) {
    function g(y) {
        if (x == y) return v;
        else return f(y);
    }
    return g;
}
```

And now some usage examples...

```
#!javascript
var env1 = emptyEnv;
var env2 = extendEnv("x", new NumVal(10), env1);
var env3 = extendEnv("y", new NumVal(5), env2);
var val = applyEnv(env2, "x"); // val will be NumVal(10)
```

## Data Structure Representation

JavaScript already has support for data structures with named fields via Objects, so we do not need the kind of kludgy representation that the authors' present in Scheme.

```
#!javascript
/**
 * Empty environment doesn't contain any variable bindings, so we'll use an empty object.
 */
var emptyEnv = null;

/**
 * Applying an environment means looking-up the value bound to a variable name.
 * Here, our environments are objects, so we just use object field lookup.
 * @param  f  an environment
 * @param  x  a variable name
 */
function applyEnv(f, x) {
    if (f === null)
        throw "Error: No binding found for variable '" + x + "'";
    if (f.savedVar && f.savedVal && f.savedEnv) {
        if (f.savedVar == x)
            return f.savedVal;
        else
           return applyEnv(f.savedEnv, x);
    }
    throw "Error: Not an environment";
}

/**
 * Extending creates an environment that adds a variable-value binding on top of an existing environment.
 * Note that we're not changing the existing environment - it is 'inside' of the extended environment.
 * @param  x  a variable name
 * @param  v  a value
 * @param  f  an environment
 */
function extendEnv(x, v, f) {
    return {
        savedVar: x,
        savedVal: v,
        savedEnv: f
    }
}
```

Since the interface is identical to that above, the exact same client-code works:

```
#!javascript
var env1 = emptyEnv;
var env2 = extendEnv("x", new NumVal(10), env1);
var env3 = extendEnv("y", new NumVal(5), env2);
var val = applyEnv(env2, "x"); // val will be NumVal(10)
```
