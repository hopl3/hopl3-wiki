# Sample Scheme/Racket Programs

The programs below are given first in Scheme/Racket and then again in JavaScript for comparison. Some of these are demonstrations of how to write simple recursive functions, while others simply show how to accomplish some common tasks in the easiest way possible.

For much more information and examples, please see the [Haskell Documentation](https://www.haskell.org/documentation) and [Learn You a Haskell for Great Good!](http://learnyouahaskell.com/) pages (also linked in our Syllabus).

If you want to see a specific example as it would be written in Scheme/Racket, then please send me a note with your request and I will add the code sample here.

## Concatenating Strings

Scheme/Racket:

```
#!haskell
"hello" ++ "world"
```

JavaScript:
```
#!javascript
"hello" + "world"
```

## Arithmetic

Haskell:

```
#!haskell
(2 + 5) * 10
```

JavaScript:

```
#!javascript
(2 + 5) * 10
```

## If-Then-Else

Note that everything in Haskell is an expression (not a statement), so there is always a return value.

Scheme/Racket:

```
#!haskell
if n > 100
    then "You win!"
    else "Try again."
```

JavaScript (must be inside a function):

```
#!javascript
if (n > 100)
    return "You win!"
else
    return "Try again."{code}
```

or use the ternary operator

```
#!javascript
n > 100 ? "You win!" : "Try again."
```

## Other Conditionals

Haskell:

```
#!haskell
if grade > 90 then "A"
else if grade > 80 then "B"
else if grade > 70 then "C"
else if grade > 60 then "D"
else "F"
```

JavaScript:

```
#!javascript
if (grade > 90) return "A";
else if (grade > 80) return "B";
else if (grade > 70) return "C";
else if (grade > 60) return "D";
else return "F";
```

## Defining & Calling Functions

Haskell:

```
#!haskell
plusFive x = x + 5

seventeen = plusFive 12
```

JavaScript:

```
#!javascript
function plusFive (x) { return x + 5; }

var seventeen = plusFive(12);
```

## Multi-Parameter Functions

Haskell:

```
#!haskell
addThem x y = x + y
```

JavaScript:

```
#!javascript
function addThem(x, y) { return x + y; }
```

## Writing a Power Function

Haskell:

```
#!haskell
power x n = if n == 0
                then 1
                else x * power x (n - 1)
```

JavaScript:

```
#!javascript
function power (x, n) {
    if (x == 0)
        return 1;
    else
        return x * power(x, n - 1);
}
```

## Creating Lists

Haskell:

```
#!haskell
[1, 2, 3, 4, 5]
```

JavaScript (as an Array, not exactly the same thing):

```
#!javascript
[1, 2, 3, 4, 5]
```

## Creating Lists with Cons

The `(:)` operator (called _cons_ because it is the list _constructor_) builds a list from an element and an existing list. In fact, a Haskell list is either the _empty list_ or an element followed by another list (think linked list). (Effectively, this is an inductive definition of what it means to be a list.)

Haskell:

```
#!haskell
listOfTwoToFive = [2, 3, 4, 5]
listOfOneToFive = 1:listOfTwoToFive
```

JavaScript (no real good equivalent, but here goes nothing):

```
#!javascript
var listOfTwoToFive = [2, 3, 4, 5];
var listOfOneToFive = [1].concat(listOfTwoToFive);
```

## List Operations

Haskell:

```
#!haskell
xs = [1,2,3,4,5]
n  = length xs  -- returns 5
x  = head xs  -- returns 1
ys = tail xs  -- returns [2,3,4,5]
```

JavaScript (no real good equivalent, but here goes nothing):

```
#!javascript
var xs = [1,2,3,4,5];
var n  = xs.length;  // returns 5
var x  = xs[0];  // returns 1
var ys = xs.slice(1,xs.length);  // returns [2,3,4,5]
```
