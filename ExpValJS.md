# Expressed Values in JavaScript

Below you can find an example implementation of the expressed value abstraction in more familiar language like JavaScript.

## Object Representation

```
#!javascript
/**
 * Initializes an object that represents a NumVal.
 * @param  n  a JavaScript number
 */
function NumVal(n) {
    if (typeof n == "number")
        this.value = n;
    else
        throw "Error: Cannot make a NumVal from a non-numeric value.";
}

/**
 * Initializes an object that represents a BoolVal.
 * @param  z  a JavaScript boolean
 */
function BoolVal(z) {
    if (typeof z == "boolean")
        this.value = z;
    else
        throw "Error: Cannot make a BoolVal from a non-boolean value.";
}

/**
 * Extracts the underlying JavaScript number from a NumVal.
 */
function expValToNum(v) {
    if (v instanceof NumVal)
        return v.value;
    else
        throw "Error: Cannot extract a number from a non-NumVal."
}

/**
 * Extracts the underlying JavaScript number from a NumVal.
 */
function expValToBool(v) {
    if (v instanceof BoolVal)
        return v.value;
    else
        throw "Error: Cannot extract a boolean from a non-BoolVal."
}
```

And now some usage examples...

```
#!javascript
var val1 = new NumVal(5);
var val2 = new BoolVal(true);
var num1 = expValToNum(val1);
var bool2 = expValToBool(val2);
```