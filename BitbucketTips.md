[Wiki Home Page](./Home.md)

# BitBucket Tips

## Prerequisites

- You must already have [Git SCM](http://git-scm.com/) installed.
- If you work better with a graphical client instead of the command line, try [SourceTree](https://www.atlassian.com/software/sourcetree/overview).

## Creating A Repository

1. Sign in to your BitBucket account using your Web browser.
2. Click on the "Create" button.
3. Name your repository and click "Create Repository". The default settings should be what you want, but to make sure...
    - the "private" checkbox should be checked
    - the repository type should be "Git"
    - optionally you can select "Scheme" as the language
4. Open your Terminal or Git Bash window, and follow BitBucket's instructions for "I'm starting from scratch"

## Issue Tracker

Here are some useful resources regarding effective use of Bitbucket's Issue Tracker.

- [Resolve issue automatically when users push code](https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html)
- ...

