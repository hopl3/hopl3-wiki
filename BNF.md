# Backus Naur Form

Backus-Naur Form is a specific syntax for writing the productions of a context-free grammar.

## Basic BNF

Productions in BNF have the general following form:

> *NON-TERMINAL* ::= *RIGHT-HAND-SIDE*

Non-terminal symbols are named and surrounded by angle-brackets (like HTML tags). For example, **<Word>** could be a non-terminal symbol. Terminal symbols are often written using explicit double-quotes to surround the concrete token(s) as in **"-"**, which represents the hyphen character itself.

An example of a BNF grammar that approximates a very small subset of English might be:

> 1. <Sentence> ::= <Subject> <Predicate> <EndPunctuation>
> 2. <Predicate> ::= <Verb>
> 3. <Predicate> ::= <Verb> <Object>
> 4. <Subject> ::= <Article> <Noun>
> 5. <Subject> ::= <Pronoun>
> 6. <Object> ::= "pie"
> 7. <Object> ::= "cake"
> 8. <Verb> ::= "smells"
> 9. <Verb> ::= "eats"
> 10. <Pronoun> ::= "He"
> 11. <Pronoun> ::= "She"
> 12. <Article> ::= "A"
> 13. <Article> ::= "The"
> 14. <Noun> ::= "dog"
> 15. <Noun> ::= "person"
> 16. <EndPunctuation> ::= "."
> 17. <EndPunctuation> ::= "!"

A valid sentence in this language is "The dog eats pie." One syntactic derivation for this sentence is:

> <Sentence>  
> => <Subject> <Predicate> <EndPunctuation>         *by rule # 1*  
> => <Article> <Noun> <Predicate> <EndPunctuation>      *by rule # 4*  
> => <Article> <Noun> <Verb> <Object> <EndPunctuation>   *by rule # 3*  
> => "The" <Noun> <Verb> <Object> <EndPunctuation>     *by rule #13*  
> => "The" "dog" <Verb> <Object> <EndPunctuation>      *by rule #14*  
> => "The" "dog" "eats" <Object> <EndPunctuation>       *by rule # 9*  
> => "The" "dog" "eats" "pie" <EndPunctuation>         *by rule # 6*  
> => "The" "dog" "eats" "pie" "."                *by rule #16*

## Extended BNF

A variety of shorthand notations have been developed and used to simplify the reading and writing of complicated grammars. We use the term EBNF to refer to variations of the original BNF style that incorporates the most widely-accepted of these shorthand notations.

### Simpler Non-Terminals

In EBNF, it is common to omit the angle brackets around non-terminal names. If using this shorthand, it is essential to retain the quotation marks around terminal symbols to clearly distinguish them from non-terminals.

### Alternation

In EBNF, we can condense multiple productions for a single non-terminal onto one line using the pipe '|' symbol. For example, the BNF grammar:

> 1. <Bit> ::= "0"
> 2. <Bit> ::= "1"

can be re-formulated as:

> 1. <Bit> ::= "0" | "1"

### Kleene Star

This notation means "zero or more occurrences of the enclosed symbols."

Consider the following BNF grammar:

> 1. <SequenceOfVars> ::= ""
> 2. <SequenceOfVars> ::= <NonEmptySequenceOfVars>
> 3. <NonEmptySequence> :: <Var>
> 4. <NonEmptySequence> :: <Var> <NonEmptySequenceOfVars>

This grammar can be re-formulated using the Kleene Star as a single EBNF production:

> 1. <SequenceOfVars> ::= {<Var>}*

### Kleene Plus

This notation means "one or more occurrences of the enclosed symbols."

Suppose we alter our original grammar to omit the empty sequence:

> 1. <SequenceOfVars> ::= <Var>
> 2. <SequenceOfVars> ::= <Var> <SequenceOfVars>

Then, we can use the Kleene Plus notation instead:

> 1. <SequenceOfVars> ::= {<Var>}+

### Separated-Lists

The authors also introduce a shorthand notation for lists of symbols that are separated (or delimited) by a specific terminal symbol(s). This notation is an extension of the Kleene star and plus notations.

For example, if we wish for our list of variable names to be comma-separated, then a regular BNF grammar might be:

> 1. <SequenceOfVars> ::= <Var>
> 2. <SequenceOfVars> ::= <Var> "," <SequenceOfVars>

While an EBNF version using the authors' separated list notation would look like:

> 1. <SequenceOfVars> ::= {<Var>}+⁽","⁾
