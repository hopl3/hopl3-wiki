# Course Wiki - Theory of Programming Languages

## Introduction

This Wiki is a repository of our collective knowledge as a class. Its primary
purpose is to provide support to all students as we go about the challenging
and enlightening task of designing and building an interpreter for an original
programming language. These pages include tips and suggestions, walk-throughs,
definitions and examples, and more. As a collaborative resource, the depth and
quality of this Wiki is precisely what we make it.

If you are looking for the Haskell EOPL3 codebase or my partial translation of
our textbook into Haskell, please visit the top-level
[eopl3-haskell repository](https://bitbucket.org/maristeopl2017/eopl3-haskell/overview)
page.

## Contributing

When making a contribution to these Wiki pages, please respect the work of
others and try to consider the "big picture" with regard to the organization
of the various pages. The easier it is to find content the better it will be.

To that end, the only modification permitted to this Home page is to *add
links to new pages* in the **Contents** section below. Please *do not* make
any other changes to the text of this Home page.

When creating new pages, please make sure to include at the top a link back to
this Home page, for convenience.

These pages are written using Markdown; please see Atlassian's
[Markdown Demo](https://bitbucket.org/tutorials/markdowndemo/overview) tutorial.

## Wiki Contents

+ [**Glossary of Terms and Concepts**](Glossary)
+ [** Frequently Asked Questions**](FAQ)
+ Tips for Interpreter Exercises
    - [Recipe for EOPL Exercises](InterpreterRecipe)
    - [Complete End-to-End Example](EndToEndExample)
    - [General Problem Solving Tips](ProblemSolving)
+ Managing Your Git Repository
    - [General Git Tips](GitTips)
    - [Bitbucket Tips](BitbucketTips)
+ Formal Syntax
    - [Backus Naur Form](BNF)
    - [Happy Grammars](Happy)
    - [Defining Language Syntax with Alex + Happy](./AlexHappy.md)
    - [SLLGEN Specifications](SLLGEN)
+ Formal Semantics
    - [Structural Operational Semantics](PlotkinOpSem)
    - [Host-Specific OpSem Notation](HostSpecOpSem)
+ Type Systems
    - [Deducing Types](DeducingTypes)
    - [Type Rules](TypeRules)
+ About Our Host Language
    - [Understanding Haskell by Example](HaskellBasics)
+ Other Host Languages
    - Implementing an Environment
        - [Environments in Java](EnvironJava)
        - [Environments in JavaScript](EnvironJS)
    - Implementing Expressed Values
        - [Expressed Values in Java](ExpValJava)
        - [Expressed Values in JavaScript](ExpValJS)
    - Implementing Abstract Syntax Trees
        - [Abstract Syntax in Java](ASTJava)
        - [Abstract Syntax in JavaScript](ASTJS)
