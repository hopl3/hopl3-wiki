[Wiki Home Page](./Home.md)

# General Git Tips

If you do better working with graphical tools, like [SourceTree](https://www.atlassian.com/software/sourcetree/overview), then feel free to use them.

However, in the real world it is _very_ common to need the command line (or _shell_) for many tasks (just as our Alums when you see them). Hence, it is worth finding time to practice at the command line whenever you have the chance.

***

## Cloning an Existing Remote Repository to Your Local Machine

1. Open your Terminal or Git Bash window.
2. Navigate to the directory underneath which you wish to store your local repository:

        #!bash
        $ cd DESIRED_CONTAINING_FOLDER

3. Enter the git-clone command:

        #!bash
        $ git clone REMOTE_REPOSITORY_URL

    - Note that you optionally can specify a folder name:

            #!bash
            $ git clone REMOTE_REPOSITORY_URL DESIRED_FOLDER_NAME

4. Navigate to your new repository directory if desired:

        #!bash
        $ cd REPO_FOLDER_NAME

***

## Making Changes

The typical _workflow_ when making changes to your Git repository is:

1. Make changes to files in your project folder or add/remove files as needed.

2. Stage these changes for your next commit:

        #!sh
        $ git add NAMES_OF_NEW_OR_CHANGED_FILES...  # if there are any
        $ git rm NAMES_OF_REMOVED_FILES...  # if there are any

3. Once everything is added/removed, freeze your commit:

        #!sh
        $ git commit -m "A useful commit message"

4. If you need to copy/upload these changes to a remote:

        #!sh
        $ git push

***

## Excluding Files

Often our project folder contains files that we do not need/want to track with Git - binary files, automatically-generated files, backup files, etc.

One way to prevent Git from tracking changes to these files is simply to avoid adding them to your repository. Here are some basic tips:

1. Avoid blindly adding everything in your project folder.

        #!sh
        $ git add *  # DO NOT DO THIS!

2. Avoid adding an entire subdirectory and its contents all at once.

        #!sh
        $ git add SUBDIR_NAME  # AVOID DOING THIS ALSO!

3. Selectively add individual files or use an extension with the wildcard.

        #!sh
        $ git add styles/*.css  # only adds CSS stylesheets located under the styles directory

4. Remove from the repository any files that you do not need/want to track.

        #!sh
        $ git rm NAME_OF_FILE_TO_STOP_TRACKING  # deletes the file from you folder
        $ git rm --cached NAME_OF_FILE_TO_STOP_TRACKING  # untracks but does not delete the file

### Ignoring Files (Pro-Tip!)

If you want more sophisticated control, it is also possible to configure Git to avoid tracking certain types of files in your repository, regardless of whether you use wildcards to _add_ files or folders. You can do this by placing a __.gitignore__ file in your repository folder and including in it the filenames/extensions or even specific paths that you wish to exclude from tracking.

Check out [this link with an example .gitignore file](http://www.bmchild.com/2012/06/git-ignore-for-java-eclipse-project.html) for Java/Eclipse.

See the [official documentation about gitignore files](http://git-scm.com/docs/gitignore) for more information.

***

## Telling Git About Other Copies of the Repository

Since Git is a distributed source management system, there can be multiple copies of a single project on different hosts - Git refers to these as "remotes." As far as Git is concerned, all repos are created equal, though they do have labels. Sometimes we wish to distinguish a particular remote repository as the "upstream" repository, meaning it is the central or original copy. This will typically be the case when cloning an existing repository, or when working with Git-hosting via BitBucket or GitHub.

1. To tell set up your your BitBucket repository as the _upstream_ copy of your project, first tell Git that your BitBucket repo is a new remote copy named "origin".

        #!sh
        $ git remote add origin URL_OF_YOUR_BITBUCKET_REPO

2. Then, the first time you want push any change to your BitBucket repo, you will need to set it as the new upstream copy (using the -u flag):

        #!sh
        $ git push -u origin master

***

## Using Branches

Git provides the ability to create multiple _branches_ of a project, each with their own commit histories, to help manage real-world development cycles and collaboration.

When making changes to a project, it is often best to commit such changes first to a branch other than the master (i.e., production) branch. Once the changes are completed (possibly after several commits), they can be reviewed and tested before merging them into the master branch.

1. To get started with branches, first create a new branch.

        #!sh
        $ git branch NEW_BRANCH_NAME

2. Next, check-out your new branch, so that all subsequent changes are made to it instead of the __master__.

        #!sh
        $ git checkout NEW_BRANCH_NAME

3. Now you can make changes to your this branch without affecting the __master__ branch of the project. Try making some changes, then stage and commit. When you are ready to push, make sure that you push your new branch by name, since Bitbucket does not yet know about it.

        #!sh
        $ git push origin NEW_BRANCH_NAME

4. Now, go back to your Bitbucket page in a Web browser and look at the details of your project. You should be able to see new branch that you just created, including one commit to that branch.

When all changes to your new branch have been thoroughly tested and reviewed, you can try merging the development branch into your __master__ branch.

1. Switch to the Master branch and pull down the latest version.

        #!sh
        $ git checkout master
        $ git pull

2. Finally, merge your branch into the __master__ and then push everything back to Bitbucket.

        #!sh
        $ git merge DESIRED_BRANCH_NAME
        $ git push

3. **IMPORTANT:** Alternatively, if you have opened a Pull Request on a branch, then you have the option of letting Bitbucket do the merge automatically when the Pull Request is approved and closed. Note that these changes will only happen on the remote Bitbucket repository, so you will need to run `git pull` in order to copy the changes down to your local repository.

***

## Creating an Empty Git Repository Locally

Sometimes it is useful to start from scratch with an empty Git repository.

1. Open your _Terminal_ or _Git Bash_ window.

2. Create a directory to hold your new project:

        #!sh
        $ mkdir DESIRED_FOLDER_NAME

3. Go into that directory (also called _changing your working directory_):

        #!sh
        $ cd DESIRED_FOLDER_NAME

4. Initialize the directory as a new, empty Git repository:

        #!sh
        $ git init

***
