# SLLGEN Specifications

The authors provide a framework called SLLGEN that combines lexical analysis and simple parser generation into a Scheme module. The name itself comes from **S**cheme **LL**(1) Parser-**GEN**erator, where LL(1) is a particular class of parser (see the [Glossary]).

Grammatical productions written in (E)BNF can be easily translated into the SLLGEN syntax be following a few basic steps.

## Structure of SLLGEN Productions

Each production in an SLLGEN grammar has the form shown below, where "LHS" and "RHS" mean the left/right-hand sides of the production.

```
( LHS_NONTERM_NAME LISTOF_RHS_SYMBOLS AST_NAME )
```

A complete EBNF grammar that describes the syntax used when writing an SLLGEN grammar can be found on page 385 in Appendix B of our textbook.

## Correspondence to BNF Productions

For some simple BNF rules, it is very easy to see how they can be re-written for SLLGEN.

```
<Bit> ::= "0"
<Bit> ::= "1"
```

The SLLGEN productions corresponding to those above are:

```
(bit ("1") on-bit)
(bit ("0") off-bit)
```

For a somewhat more interesting example, suppose we have the following BNF production:

```
<FullName> ::= <LastName> "," <FirstName> <MiddleInitial>
```

In SLLGEN, an equivalent production could look like:

```
(fullname (lastname "," firstname middleinitial) a-name)
```

In the list above, the non-terminal symbols are **lastname**, **firstname**, **middleinitial**. The only terminal symbol is **","** and the abstract syntax name is **a-name**. Of course, we would have to define these other non-terminals elsewhere in the grammar.

## More Usage Examples

The examples below demonstrate some of the other features of the SLLGEN syntax. In each case, the BNF production(s) are given first and then followed by the equivalent SLLGEN production(s).

### Kleene Star

Consider the following EBNF production involving Kleene star:

```
<SequenceOfVars> ::= {<Var>}*
```

The same rule could be written in the SLLGEN syntax as:

```
(sequenceOfVars ((arbno var)) some-vars)
```

### Kleene Plus

In order to give the same result as Kleene plus:

```
<SequenceOfVars> ::= {<Var>}+
```

we will need to have at least one **Var** non-terminal guaranteed on the right-hand side of the SLLGEN rule:

```
(sequenceOfVars (var (arbno var)) some-vars)
```

### Separated-Lists

Consider a comma-separated list of variable names, defined by the EBNF production:

```
<SequenceOfVars> ::= {<Var>}+⁽","⁾
```

In an SLLGEN grammar, we could write this as:

```
(sequenceOfVars (separated-list var ",") some-vars)
```

## A Larger Example

We can describe the syntax of an SLLGEN grammar using an SLLGEN grammar itself!

The EBNF grammar shown on page 385 of the textbook could be re-written in the SLLGEN form as:

```
#!racket
(define the-sllgen-grammar-itself
  '((grammar ("(" (arbno production) ")") a-grammar)
    (production ("(" lhs "(" (arbno rhs-item) ")" prod-name) a-production)
    (lhs (symbol) an-lhs)
    (rhs-item (symbol) sym-item)
    (rhs-item (string) str-item)
    (rhs-item ("(" "arbno" (arbno rhs-item) ")") arbno-item)
    (rhs-item ("(" "separated-list" (arbno rhs-item) string ")") seplist-item)
    (prod-name (symbol) a-prod-name))
```
