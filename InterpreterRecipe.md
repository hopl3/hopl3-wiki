# How to Approach an EOPL Interpreter Exercise

Depending upon the exercise itself, one or more of these steps may be omitted, or may differ slightly. This is intended as an approximate guideline for thinking about these problems.

## General Recommendations

+ Be sure you get a head start thinking about the problem(s), as a correct implementation will depend on your understanding of the requirements and the tools you will use to achieve them.

+ To avoid a proliferation of files, keep all your work in the Haskell source files. This includes the grammar and semantic rules, which should be typed as comments in the appropriate source file.

+ For each snippet/block of implementation, always ensure that the accompanying comment includes the Exercise number.

+ At all times, be sure you know which language you are looking at or working with. Is it source language? Or, host language?

## Test Cases

Believe it or not, developing your test cases up front is generally a good practice. The process of crafting test cases can inform considerations like interface design, contracts (including pre- and post-conditions), and your expectations in general.

Place all new test cases in the appropriate Haskell file for the language, inside the `Test` directory. For example, when modifying the _LET_ language, add any new test cases to the `Test/LET.hs` file.

Each test case is implemented as a value of the `Test` data type. Values of type `Test` typically include:

+ the name of the test case (as a String)
+ the test program source code (as a String)
+ the solution as a Haskell value (of appropriate type)

Be sure to write more than one test case, and try to cover different scenarios.

+ __Simple cases__ - Include a few programs that are as short as possible to test each new language feature.
+ __Compound cases__ - The way that new features interact with existing features is also important and sometimes unexpected. Include some longer programs that employ new features in various contexts. For example, if you add a new expression that returns a new kind of value, you should test binding the returned value to a variable, passing the returned value into another operator or procedure, returning the value from a procedure, and anything else you can think of.

## Syntax

Drawing on sample programs or descriptions provided by the authors, or your own test programs, identify what the concrete syntax looks like. Specifically, determine what are the terminal and non-terminal symbols, i.e. the left- and right-hand sides of the rule. If you want to be sure that a rule is correct, try writing out a syntactic derivation for a short sample expression/program using your rule; if you cannot derive the expression, then the rule is not correct.

Next, identify the "abstract syntax components" of the concrete syntax. Specifically, each production is a new kind of node in the abstract syntax tree, and each non-terminal appearing on the right-hand side of the production is an attribute of the node.

Syntactic changes typically require edits to the following source files:

+ `LexSpec.x` - new token types such as reserved words and predefined operators must be added here.

+ `Grammar.y` - new grammatical productions, including the abstract syntax construction, must be added here

+ `AST.hs` - new kinds of abstract syntax tree nodes are added as new constructors (e.g., for the `Exp` type)

## Semantics

All your semantic edits will go in the `Interp.hs` file. Most things will go inside the `value_of` procedure, with the exception of any helper functions you may need to modify or create.

It can help to first write out an inference rule for the operational semantics of your new/modified expressions. I recommend writing this first in the host-agnostic style I described in class. Recall that an inference rule typically has one or more hypotheses and a conclusion, each of which is a judgment having a left and right side. Always start with the left-hand side of the conclusion, and the work backward to determine what information you need in order to compute that.

For example, suppose we need to write a rule for evaluating the expression "twice(exp1)". We can start with something like __<`twice(exp1)`,p> ⇓__, where we leave the right-hand side empty. We then write a hypothesis that describes how to compute the value of each sub-expression, such as __<`exp1`,p> ⇓ ⌈_n_⌉__. Finally, we can fill-in the final answer on the right-hand side of the conclusion, as in __<`twice(exp1)`,p> ⇓ ⌈2×n⌉__. Generally speaking, you will need one hypothesis for each sub-expression.

You can translate a rule from the host-agnostic style into a Haskell-specific style by substituting the appropriate function and data type names in for the AST and ExpVal portions. In this style, the above rule may be rewritten with a hypothesis of `(NumVal n) = value_of exp1 p` and a conclusion of `value_of (TwiceExp exp1) p = NumVal (2 * n)`.

+ Tip: When dealing with arbitrary lists of sub-expressions, a properly rigorous way to specify the behavior is to use induction.

    First, identify a base case, such as when there is either zero or exactly one sub-expression. Write one inference rule for this case.

    Then, define any additional inference rule inductively/recursively. That is, it is okay to refer to the rule itself. Think of list processing... we have a base case of "empty list" and then we have an additional case where we operate on the "head" before recurring on the "tail".

If your semantic rule is correct, then turning that into actual implementation tends to be straightforward.

Finally, don't forget that expressions always evaluate to some kind of expressed value in the language, so don't return raw Haskell values! Always wrap such values in an value of the ExpVal type and return that.

## Supporting Structures [as needed]

Changes or additions to the representations of supporting structures will go in one of other files, such as the `Val.hs` or `Environments.hs` modules.

+ __Expressed Values__ - To add a new kind of expressed value, you will need to add a new constructor to the `ExpVal` data type in `Val.hs` file, and add a new case to the `Show` instance implementation.

+ __Environment__ - The interface and several representations for the environment are defined in `Environment.hs`. Most likely you will not have to modify this file (only a few exercises require this).
+ __Miscellaneous__ - In some cases, you may find it useful or necessary to add a new data structure that does not already exist. Always consider what will be your interface, and how will you represent the data. Implement your data type in a new module. We will see and example of this in the languages of Chapter 4, and until then you should not have to worry about it.

## Debugging!

You're almost there now! Don't give up!

+ Build and run the Test program by running the build.sh script. Optionally, run the REPL as well to experiment interactively.
+ If you have any "failed" tests, then try to make sense of the error message and determine where the problem originates. Is the test case written incorrectly? Did you expect the wrong answer? Is your syntactic definition wrong? Does the error occur in `value_of`? Etc.

Good luck!
